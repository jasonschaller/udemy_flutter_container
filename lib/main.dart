import 'package:flutter/material.dart';

void main() {
  runApp(application());
}

class application extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Just for practice',
      home: new Scaffold(
        body: new Container(
          //color: Colors.pink,
          child: new Text('This is my Container'),
          height: 300.00,
          width: 300.00,
          alignment: Alignment.center,
          padding: const EdgeInsets.all(20.0),
          decoration: new BoxDecoration(
            color: Colors.green,
          ),
          transform: new Matrix4.rotationZ(0.5),
          foregroundDecoration: new BoxDecoration(
            color:  Colors.deepOrange,
          ),
        ),
      ),
    );
  }
}
